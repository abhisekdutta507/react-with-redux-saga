import React from 'react';
import './App.css';

import { Provider } from 'react-redux';
import store from './stores';

import Header from './components/Header';
import ImageGrid from './components/ImageGrid';
import LoadMore from './components/LoadMore';

function App() {
  return (
    <Provider store={store}>
        <Header />
        <ImageGrid />
        <LoadMore />
    </Provider>
  );
}

export default App;
