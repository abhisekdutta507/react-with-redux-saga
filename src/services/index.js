const key = 'Vr_3xYtaTnH6lhN83i0vjH2UKrIUDU1L1c67ta8N2B0';
const URL = 'https://api.unsplash.com/photos';

const fetchImages = async page => {
  const response = await fetch(`${URL}/?client_id=${key}&per_page=14&page=${page}`);
  const data = await response.json();
  if(response.status >= 400) {
    throw new Error(data.errors);
  }
  return data;
};

export { fetchImages };
