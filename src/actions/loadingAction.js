import { IMGAES } from '../constants';

const loadImages = () => ({
  type: IMGAES.LOAD
});

const setImages = images => ({
  type: IMGAES.LOAD_SUCCESS,
  images
});

const setErrors = error => ({
  type: IMGAES.LOAD_FAIL,
  error
});

export {
  loadImages,
  setImages,
  setErrors
};
