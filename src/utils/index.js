function wait (d) {
  return new Promise(n => {
    setTimeout(() => {n();}, 100 * d);
  });
}

export {
  wait
};
