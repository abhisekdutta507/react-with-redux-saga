import { IMGAES } from '../constants';
// import { act } from 'react-dom/test-utils';

const pageReducer = (state = 1, action) => {
  switch (action.type) {
    case IMGAES.LOAD_SUCCESS: return state + 1;
    default: return state;
  }
}

export default pageReducer;
