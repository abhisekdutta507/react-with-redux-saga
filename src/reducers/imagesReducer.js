import { IMGAES } from '../constants';

const imagesReducer = (state = [], action) => {
  if (action.type === IMGAES.LOAD_SUCCESS) {
    return [ ...state, ...action.images ];
  }
  return state;
}

export default imagesReducer;