import { IMGAES } from '../constants';

const errorReducer = (state = null, action) => {
  switch (action.type) {
    case IMGAES.LOAD_FAIL: return action.error;
    case IMGAES.LOAD:
    case IMGAES.LOAD_SUCCESS: return null;
    default: return state;
  }
}

export default errorReducer;