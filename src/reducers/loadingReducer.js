import { IMGAES } from '../constants';

const loadingReducer = (state = false, action) => {
  switch (action.type) {
    case IMGAES.LOAD: return true;
    case IMGAES.LOAD_SUCCESS: return false;
    case IMGAES.LOAD_FAIL: return false;
    default: return state;
  }
}

export default loadingReducer;