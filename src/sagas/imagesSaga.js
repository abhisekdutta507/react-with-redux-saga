import { takeEvery, put, select, call } from 'redux-saga/effects';

import { IMGAES } from '../constants';
import { fetchImages } from '../services';
import { setImages, setErrors } from '../actions/loadingAction';

const getPage = state => state.nextPage;

export default function* watchImagesLoad() {
  yield takeEvery(IMGAES.LOAD, handleImagesLoad);
}

function* handleImagesLoad() {
  try {
    const page = yield select(getPage);
    const images = yield call(fetchImages, page);
    yield put(setImages(images));
  } catch(error) {
    yield put(setErrors(error.toString()));
  }
}
