import React, { Component } from 'react';
import { connect } from 'react-redux';
import './styles.css';

import { loadImages } from '../../actions/loadingAction';

import { wait } from '../../utils';

class ImageGrid extends Component {

    async componentDidMount() {
      await wait(5);
      this.props.loadImages();
    }

    componentDidUpdate(p, c) {
        // const { images } = this.props;
    }

    render() {
        const { images } = this.props;
        return (
            <div className="content">
                <section className="grid">
                    {images.map((image, index) => (
                        <div
                            key={`${image.id}--${index}`}
                            data-key={`${image.id}--${index}`}
                            className={`item item-${Math.ceil(
                                image.height / image.width,
                            )}`}
                        >
                            <img
                                src={image.urls.small}
                                alt={image.user.username}
                            />
                        </div>
                    ))}
                </section>
            </div>
        );
    }
}

const mapStateToProps = ({ isLoading, images, error, nextPage }) => ({
    isLoading, images, error, nextPage
});

const mapDispatchToProps = dispatch => ({
    loadImages: () => dispatch(loadImages()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImageGrid);
