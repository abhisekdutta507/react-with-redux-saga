import React, { Component } from 'react';
import { connect } from 'react-redux';
import './styles.css';

import { loadImages } from '../../actions/loadingAction';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedoAlt } from '@fortawesome/free-solid-svg-icons';

class LoadMore extends Component {

  componentDidUpdate(p, c) {
      // const { images, isLoading } = this.props;
  }

  render() {
    const { loadImages, isLoading } = this.props;

    return (
      <div className="content">
        <section className="pb-25">
          <FontAwesomeIcon className={`fa grey`} icon={faRedoAlt} onClick={() => loadImages()} spin={isLoading} size="2x" />
        </section>
      </div>
    );
  }
}

const mapStateToProps = ({ isLoading, images, error, nextPage }) => ({
    isLoading, images, error, nextPage
});

const mapDispatchToProps = dispatch => ({
    loadImages: () => dispatch(loadImages()),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoadMore);
